import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth-credentials.dto';
import { jwtPayload } from './jwt-payload.interface';
import { UserRepository } from './user.repository';

@Injectable()
export class AuthService {

    private logger = new Logger('AuthService');

    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        private jwtService: JwtService,
    ){}

    async signUp(authCredentialsDTO: AuthCredentialsDTO){
        return await this.userRepository.signUp(authCredentialsDTO);
    }

    async signIn(authCredentialsDTO:AuthCredentialsDTO): Promise<{accessToken: string}>{
        const username= await this.userRepository.validateUserPassword(authCredentialsDTO);

        if(!username){
            throw new UnauthorizedException ('Invalid Credentials');
        }

        const payload: jwtPayload = { username };
        const accessToken= await this.jwtService.sign(payload);
        this.logger.debug(` JWT Token Generated with Payload: ${JSON.stringify(payload)}`);

        return { accessToken };
    }    
}
