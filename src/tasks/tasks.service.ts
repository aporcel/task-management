import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
import { CreateTaskDTO } from './dto/create-task.dto';
import { GetTaskFilterDTO } from './dto/get-task-filter.dto';
import { TaskStatus } from './task-status.enum';
import { Task } from './task.entity';
import { TaskRepositoy } from './task.repository';

@Injectable()
export class TasksService {


    constructor(
        @InjectRepository(TaskRepositoy)
        private taskRepository: TaskRepositoy,
    ) { }


    async getTaskById(id: number, user:User): Promise<Task> {
        const found = await this.taskRepository.findOne({where: {id, userId: user.id } });

        if (!found) {
            throw new NotFoundException(`task with id ${id} not found`);
        }

        return found;

    }


    async createTask(createTaskDTO:CreateTaskDTO, user: User): Promise<Task>{
        return this.taskRepository.createTask(createTaskDTO, user);
    }


    async deleteTask(id:number, user: User): Promise<void>{
        const result = await this.taskRepository.delete({ id, userId: user.id });
        if(result.affected===0){
            throw new NotFoundException(`task with id ${id} not found`);
        }
    }

    async updateTaskStatus(id: number, status: TaskStatus, user:User): Promise<Task> {
        const task= await this.getTaskById(id, user);
        task.status=status;
        await task.save();
        return task;
    }

    async getTasks(filterDTO:GetTaskFilterDTO, user:User): Promise<Task[]>{
        return this.taskRepository.getTasks(filterDTO, user);

    }
    


}
